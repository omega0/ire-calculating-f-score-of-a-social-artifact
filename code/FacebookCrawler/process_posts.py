# Created by Swapnil P on 13/4/15.
#!/usr/bin/python2
from time import sleep
import dateutil.parser
import datetime
import os
import ast
import requests

#Users to Crawl
users = ['natgeo','nytimes','bbcnews','cnn','yahoonews','wsj','ESPN','Reuters','TheEconomist','FoxNewsLive','Channel4News','APNews','CBSNlive','NBCNews','abcnews']

access_token = '1383753075284064|80a4df9598a89902a15ee4525f8897eb'

loc = '../'
if not os.path.exists(loc):
 os.mkdir(loc)

def process_post(post,user):
    post_dictionary = {}

    #======Calculating number of likes=======
    sleep(1)
    score = {}
    str1    = 'https://graph.facebook.com/'
    str2    = '/likes?summary=1&access_token='+access_token
    try:
        score['likes'] = requests.get(str1+post['id']+str2).json()['summary']['total_count']
    except:
        print "Connection Error, Sleeping for 10 sec"
        sleep(10)
        process_post(post=post,user=user)
        return

    #=====Calculating number of comments=====
    sleep(1)
    str2 = '/comments?summary=1&filter=toplevel&access_token='+access_token
    try:
        score['comments'] = requests.get(str1+post['id']+str2).json()['summary']['total_count']
    except:
        print "Connection Error, Sleeping for 10 sec"
        sleep(10)
        process_post(post=post,user=user)
        return

    post_dictionary['id'] = post['id']

    if post.has_key('link'):
        post_dictionary['url'] = post['link']

    if post.has_key('message'):
        post_dictionary['text'] = post['message']

    #====Calculating shares count of this post====
    try:
        score['shares'] = post['shares']['count']
    except (KeyError, TypeError):
        score['shares'] = 0

    score['val'] = score['likes'] + 2*score['comments'] + 3*score['shares']
    post_dictionary['score'] = score

    #====Extracting additional artifacts {pic, video link}====
    media = []
    if post.has_key('picture'):
        media.append(post['picture'])
    post_dictionary['media'] = media

    f = open(loc+user,'a')
    f.write(str(post_dictionary)+'\n')
    f.close()

for user in users:
    print "Processing for",user
    fp = open('posts/'+user)
    while True:
        post = fp.readline();
        if len(post) == 0:
            break    
        post = ast.literal_eval(post)
        process_post(post=post,user=user)