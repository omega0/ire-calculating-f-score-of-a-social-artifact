#!/usr/bin/python2
import facebook
import requests
from time import sleep
import dateutil.parser
import datetime
import pytz
import os

access_token = 'CAACEdEose0cBACNvJELfQoagePjRLbQodbWq0iDgFiXZCuJ4s05SqMPlAlae1GaGF7OCpZApgDJ72ZCgRFdpZB9IjEZBzCHsIjHAv40lVV22Gt0J1ZBAbPhj9onMRGo8pKyXk6UffRV3WjJYpCFX2u83veJzszAoxEZB5LLvgFdZCEOw6SV7fDrrTLp9BvwE19MZBmdN11LK5x6l02524ZCpn6lXK1wXgD19ZA5rJHBnNNZAZBgZDZD'

#Users to Crawl
users = ['natgeo','nytimes','bbcnews','cnn','yahoonews','wsj','ESPN','Reuters','TheEconomist','FoxNewsLive','Channel4News','APNews','CBSNlive','NBCNews','abcnews']

num_post = 0

loc = './posts/'
if not os.path.exists(loc):
 os.mkdir(loc)
def some_action(post,user):
    if post['type'] == 'status':
        return
    global num_post
    num_post = num_post + 1
    f = open(loc+user,'a')
    f.write(str(post)+'\n')
    f.close()


graph = facebook.GraphAPI(access_token)
for user in users:
 print "Crawling for",user
 profile = graph.get_object(user)
 posts = graph.get_connections(profile['id'], 'posts')
 while True:
    try:
        [some_action(post=post,user=user) for post in posts['data']]
        if len(posts['data'])>0:
            ctime = dateutil.parser.parse(posts['data'][-1]['created_time'])
            print "Crawled till", ctime
        if num_post >=  1500:
            num_post = 0
            break
        sleep(1)
        posts = requests.get(posts['paging']['next']).json()
    except KeyError:
        num_post = 0;
        break