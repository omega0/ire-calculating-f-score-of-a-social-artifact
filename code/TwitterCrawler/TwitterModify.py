import json

f = open("ABC.json", "r")
arr = f.readlines()
out = []
mx = 0
for i in arr:
    try:
        l = json.loads(i[:-2])
        mx = max(mx, l["score"])
    except:
        pass

for i in arr:
    try:
        l = json.loads(i[:-2])
        l["score"] = float(l["score"]) / mx
        out.append(l)
    except:
        pass

ans = open("output.json","w")
ans.write(json.dumps(out))
ans.close()
