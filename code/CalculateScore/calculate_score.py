"""
Author : Amar Budhiraja
Modification : Somya
Calculates the social score of a web artifact.


To run : python calculate_score.py <artifact terms seperated by space>


"""


import requests
import json
import sys

csvfile = open("pending.csv","a")

numbers_of_terms = len(sys.argv)-1

artifact = ""

for i in xrange(numbers_of_terms):
	artifact += sys.argv[i+1] + "+"

artifact = artifact[:-1]

csvfile.write(artifact+',')

request_url = 'http://localhost:8983/solr/collection1/select?q='+ artifact +'&sort=score+desc&start=0&rows=10&wt=json'

print request_url

solr_response = requests.get(request_url)
print solr_response
if solr_response.status_code == 200:


	json_dump =  json.loads(solr_response.content)
	list_of_documents = json_dump[u'response'][u'docs']
	print list_of_documents[0]
	score = 0.0

	for doc in list_of_documents:
		score += float(doc[u'score'][0])


	print "The Artifact has a social score of ", str(score)

	print "Artifact : Score"
	for doc in list_of_documents:
		if u'url' in doc.keys() and type(doc[u'url']) is not list:
			#print doc[u'url']+" : "+ doc[u'score'][0]
			x = str(doc[u'url'])+' '
			csvfile.write(x)
		else:
			if u'url' in doc.keys():
				print doc[u'url'][0]+" : "+ doc[u'score'][0]
				x = str(doc[u'url'][0])+' '
				csvfile.write(x)
	
	csvfile.write(','+str(score)+'\n')
	csvfile.close()
	execfile("print_to_html.py")
else:
	print "Server Down. Please try later on contact The Justice League to solve this for you! :P"
