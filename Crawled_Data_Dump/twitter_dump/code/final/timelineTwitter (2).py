"""
Created by Amar Budhiraja
How to run : python timelineTwitter.py <screen_namee> <number_of_tweets>
modification : somya
"""
from twitter import *
import urllib2
import sys
import json
import time
#Setting the proxy settings
your_squid_server = urllib2.ProxyHandler({'http': 'proxy.iiit.ac.in:8080', 'https': 'proxy.iiit.ac.in:8080'})
new_opener = urllib2.build_opener(your_squid_server)
urllib2.install_opener(new_opener) 


#Taking screen names and count of tweets required
#Count can't be greater than 3200 
screen_name = sys.argv[1]


# these tokens are necessary for user authentication
# (created within the twitter developer API pages)
# DONT SHARE IT ANYONE !!

consumer_key = "1HGNRTUFvMZ5VShQ9rWFIftXw"
consumer_secret = "k4jpkAs1bN9cVQRQarFBEMkvOPL9Nl9q1ibg6Lm8u0la2pOqFO"
access_key = "219714543-WsWWcxD7V00vvAB2HI68yqGc6PaS4AHT2sIfPZBK"
access_secret = "AhKC7ITJCftcd6aV13Yz6qggqOGFGjgdXA8FmEVYeW4Me"


# create twitter API object
auth = OAuth(access_key, access_secret, consumer_key, consumer_secret)
twitter = Twitter(auth = auth)

#Retrieving Tweets for the given screen name
statuses = twitter.statuses.user_timeline(screen_name = sys.argv[1])

favorite_count_factor = 1
retweeter_followers_count_factor = 1

print """Format of DUMP IS : {id = tweet_id, score = score ,
 			text = tweet_text, entities = tweet_entities }"""
for tweet in statuses:
	time.sleep(50)
	score = 0 
	_retweets = twitter.statuses.retweets(id=tweet['id_str'])
	score = favorite_count_factor * int(tweet['favorite_count'])
	score += tweet['retweet_count']

	# following snippet is inconsistent :/ (possible reasons : id exceeding python int in retweets id (id_str) ; ) 
	for retweet in _retweets:
		score += int(retweet['user']['followers_count'])+1
	
	tweet_dictionary = {}
	tweet_dictionary['id_str'] = tweet['id_str']
	tweet_dictionary['score'] = score
	tweet_dictionary['text'] = tweet['text']
	#tweet_dictionary['entities'] = tweet['entities']
	media = []
	
	try:
		for _media in tweet['entities']['media']:
			media.append(_media['media_url'])
	except:
		pass
	tweet_dictionary['media'] = media
	
	url = []
	try:
		for _url in tweet['entities']['urls']:
			url.append(_url['expanded_url'])
	except:
		pass
	tweet_dictionary['url'] = url
	print json.dumps(tweet_dictionary)
