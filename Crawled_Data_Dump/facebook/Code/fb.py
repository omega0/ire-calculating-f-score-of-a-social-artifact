# Created by Swapnil P on 26/3/15.
#!/usr/bin/python2
import facebook
import requests
from time import sleep
import dateutil.parser
import datetime
import pytz
import os

access_token = '1383753075284064|f3894b2ddd4df75438406d459323c2ed'
num_post = 0

#Users to Crawl
users = ['washingtonpost','wsj','ESPN','Reuters','TheEconomist','FoxNewsLive','Channel4News','APNews','CBSNlive','NBCNews','abcnews','natgeo','nytimes','bbcnews','cnn','yahoonews']

loc = './fbdata/'
if not os.path.exists(loc):
 os.mkdir(loc)

def process_post(post,user):
    sleep(1)
    global num_post
    if post['type'] != 'status':
        post_dictionary = {}

        post_dictionary['id'] = post['id']

        if post.has_key('link'):
            post_dictionary['url'] = post['link']
            if 'facebook' not in post['link']:
                num_post = num_post + 1

        if post.has_key('message'):
            post_dictionary['text'] = post['message']

        #======Calculating number of likes=======
        score = {}
        str1    = 'https://graph.facebook.com/'
        str2    = '/likes?summary=1'
        try:
            score['likes'] = requests.get(str1+post['id']+str2).json()['summary']['total_count']
        except KeyError:
            score['likes'] = 0
        except ConnectionError:
            sleep(250)
            score['likes'] = 0

        #=====Calculating number of comments=====
        str2 = '/comments?summary=1&filter=toplevel'
        try:
            score['comments'] = requests.get(str1+post['id']+str2).json()['summary']['total_count']
        except KeyError:
            score['comments'] = 0
        except ConnectionError:
            sleep(250)
            score['comments'] = 0

        #====Calculating shares count of this post====
        try:
            score['shares'] = post['shares']['count']
        except (KeyError, TypeError):
            score['shares'] = 0

        score['val'] = score['likes'] + 2*score['comments'] + 3*score['shares']
        post_dictionary['score'] = score

        #====Extracting additional artifacts {pic, video link}====
        media = []
        if post.has_key('picture'):
            media.append(post['picture'])
        post_dictionary['media'] = media

        f = open(loc+user,'a')
        f.write(str(post_dictionary)+'\n')
        f.close()

graph = facebook.GraphAPI(access_token)
for user in users:
    print "Crawling for",user
    profile = graph.get_object(user)
    posts = graph.get_connections(profile['id'], 'posts')
    while True:
        try:
            [process_post(post=post,user=user) for post in posts['data']]
            if len(posts['data'])>0:
                ctime = dateutil.parser.parse(posts['data'][-1]['created_time'])
                print "Crawled till", ctime
            if num_post >=  1000:
                num_post = 0
                break	
            sleep(1)
            posts = requests.get(posts['paging']['next']).json()
        except KeyError:
            sleep(30)
            break
